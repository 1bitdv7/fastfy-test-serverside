class PostController {
  async index(req, reply) {
    return reply.view("pages/index.ejs", { Hello: "Ola" });
  }

  async book(req, reply) {
    const { book } = req.params;
    if(!book){
        throw new Error('error invalid book')
    }
    return reply.view("pages/index.ejs", { Hello: "book routes" });
  }
}

export default new PostController();
