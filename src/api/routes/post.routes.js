import postController from "../module/Post/post.controller.js"

export default async  (fastify,opts,done) =>{
    fastify.get('/',postController.index)
    fastify.get('/book/:book',postController.book)
}