import fastify from "fastify";
const server = fastify({ logger: true });
import postRoutes from "./api/routes/post.routes.js";
import fastifySession from "@fastify/session";
import fastifyCookie from "@fastify/cookie";
import ejs from "ejs";
import * as url from "url";

const PORT = 5000;
await server.register(import("@fastify/compress"), {
    global: true,
});
server.register(fastifyCookie);
server.register(fastifySession, {
  secret: "uyuiyaiydiasud878d7678a5d67asdbduasd768",
});
server.addHook("onSend", async function (request, reply) {
  reply.headers({
    "Cache-Control": "no-store",
    Pragma: "no-cache",
  });
});

server.register(import("@fastify/static"), {
  root: url.fileURLToPath(new URL("..", import.meta.url)) + "public/",
});
server.register(import("@fastify/view"), {
  engine: {
    ejs: ejs,
  },
  templates: url.fileURLToPath(new URL(".", import.meta.url)) + "view/",
});

await server.register(postRoutes);

const start = async () => {
  try {
    await server.listen({ port: PORT }, (error, adress) => {
      if (error) {
        server.log.error(err);
        process.exit(1);
      }
    });
  } catch (error) {
    server.log.error(error);
    process.exit(1);
  }
};
start();
